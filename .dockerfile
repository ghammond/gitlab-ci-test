FROM ubuntu:bionic

VOLUME /scratch

RUN apt-get update -qq && \
    apt-get install -y git make gfortran

WORKDIR /scratch
COPY . .
CMD ./build-hello-world.sh
